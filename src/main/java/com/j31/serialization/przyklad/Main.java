package com.j31.serialization.przyklad;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Main {
    public static void main(String[] args) {
        // json -
        // xml
        Car samochodzik = new Car("Maluszek", 0.6);
        /*
        {
            "name": "Maluszek",
            "engineCapacity": 0.6
        }
        * */
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String jsonSamochodzik = objectMapper.writeValueAsString(samochodzik);

            System.out.println(jsonSamochodzik);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
