package com.j31.serialization.przyklad;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Car {
    private String name;
    private double engineCapacity;
//    private Person driver;
}
