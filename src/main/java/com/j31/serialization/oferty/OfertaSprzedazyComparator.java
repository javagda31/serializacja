package com.j31.serialization.oferty;

import java.util.Comparator;

public class OfertaSprzedazyComparator implements Comparator<OfertaSprzedazy> {
    private boolean reverse;

    public OfertaSprzedazyComparator(boolean reverse) {
        this.reverse = reverse;
    }

    @Override
    public int compare(OfertaSprzedazy ofertaSprzedazy, OfertaSprzedazy t1) {
        if (!reverse) {
            return Double.compare(ofertaSprzedazy.getCena(), t1.getCena());
        } else {
            return Double.compare(t1.getCena(), ofertaSprzedazy.getCena());
        }
    }
}
