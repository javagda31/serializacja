package com.j31.serialization.oferty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor // POJO
public class OfertaSprzedazy {
    private String nazwa;
    private double cena;
}
