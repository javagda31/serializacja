package com.j31.serialization.oferty;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.*;
import java.lang.reflect.Type;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        List<OfertaSprzedazy> lista = new ArrayList<>();
        załadujOfertyDoListyGson(lista);
        String komenda;

        do {
            komenda = scanner.next();

            if (komenda.equalsIgnoreCase("dodaj")) {
                String nazwa = scanner.next();
                double cena = scanner.nextDouble();

                OfertaSprzedazy ofertaSprzedazy = new OfertaSprzedazy(nazwa, cena);
                lista.add(ofertaSprzedazy);
                System.out.println("Dodano " + nazwa);
            } else if (komenda.equalsIgnoreCase("listuj")) {
                lista.forEach(System.out::println);
            } else if (komenda.equalsIgnoreCase("sortuj")) {
                String kierunek = scanner.next();
                if (kierunek.equalsIgnoreCase("rosnaco")) {
                    Collections.sort(lista, new OfertaSprzedazyComparator(false));
                } else {
                    Collections.sort(lista, new OfertaSprzedazyComparator(true));
                }
            }

        } while (!komenda.equalsIgnoreCase("quit"));

        zapiszListęDoPlikuGson(lista);
        // kryterium sortowania.
//        Queue<Integer> queue = new PriorityQueue<>();
//        queue.add(5);
//        queue.add(25);
//        queue.add(3);
//        queue.add(1);
//        queue.add(8);

//        for (Integer integer : queue) {
//            System.out.println(integer);
//        }
//        while (!queue.isEmpty()){
//            System.out.println(queue.poll());
//            System.out.println(queue.);
//        }
    }

    /**
     * W argumencie przyjmuję pustą listę ofert. Jeśli wczytanie z pliku się powiedzie,
     * to do listy zostaną dopisane nowe (wczytane) elementy.
     *
     * @param lista
     */
    private static void załadujOfertyDoListy(List<OfertaSprzedazy> lista) {
        ObjectMapper mapper = new ObjectMapper(); // konieczność
        try {
            // deklaracja i wczytanie listy z pliku
            List<OfertaSprzedazy> wczytanaLista = mapper.readValue(new File("oferty.niekonieczniejson"), new TypeReference<List<OfertaSprzedazy>>() {
            });
            ///////////////////
            lista.addAll(wczytanaLista); // wszystkie encje z pliku zostały załadowane, więc przepisuję je do listy z argumentu

        } catch (IOException e) {
            System.err.println("Nie mogę wczytać pliku.");
        }
    }

    private static void załadujOfertyDoListyGson(List<OfertaSprzedazy> lista) {
        Gson gson = new Gson();

        StringBuilder builder = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new FileReader("oferty.gson.json"))) {
            // dla każdej linii z pliku, dopisz ją do buildera
            reader.lines().forEach(builder::append);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        String jsonFromFile = builder.toString();

        Type deserializowanyTyp = new TypeToken<List<OfertaSprzedazy>>(){}.getType();
        List<OfertaSprzedazy> list = gson.fromJson(jsonFromFile, deserializowanyTyp);
        lista.addAll(list);
    }

    private static void zapiszListęDoPliku(List<OfertaSprzedazy> listaDoZapisu) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(new File("oferty.niekonieczniejson"), listaDoZapisu);

            // serializacja do String
            String json = mapper.writeValueAsString(listaDoZapisu);
        } catch (IOException e) {
            System.err.println("Nie mogę zapisać listy.");
        }
    }

    private static void zapiszListęDoPlikuGson(List<OfertaSprzedazy> listaDoZapisu) {
        Gson gson = new Gson();
        // użyj gson do serializacji do JSON
        String jsonString = gson.toJson(listaDoZapisu);
        try (PrintWriter writer = new PrintWriter(new FileWriter("oferty.gson.json"))) {
            writer.println(jsonString);
        } catch (IOException e) {
            System.err.println("Nie mogę zapisać pliku");
        }
    }


}
