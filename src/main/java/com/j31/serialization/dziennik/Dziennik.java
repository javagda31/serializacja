package com.j31.serialization.dziennik;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Dziennik {
    private List<Student> students = new ArrayList<>();

    public Dziennik() {
        wczytajZDysku();
    }

    public void dodajStudenta(Student st) {
        students.add(st);
    }

    public void usunStudenta(Student st) {
        students.remove(st);
    }

    private void wczytajZDysku() {
        ObjectMapper objectMapper = new ObjectMapper();
//        objectMapper.readValue(new File("memory.json"), Student.class);

        try {
            List<Student> list = objectMapper.readValue(
                    new File("memory.json"),
                    new TypeReference<List<Student>>() {
                    }
            );
            this.students = list;

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void zapiszNaDysk() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            objectMapper.writeValue(new File("memory.json"), students);
        } catch (IOException e) {
            System.err.println("Nie udało się zapisać danych na dysk.");
        }
    }

    /**
     * Metoda zwraca Optional (pudełko) ze studentem. Jeśli student jest dostępny i został znaleziony to zostanie
     * zwrócony w formie pudełka. W pudełku mamy studenta i musimy przed wyciągnięciem sprawdzić czy tam jest.
     *
     * @param nrIndeksu
     * @return
     */
    public Optional<Student> zwrocStudenta(String nrIndeksu) {
        for (Student student : students) {
            if (student.getIndeksNumber().equals(nrIndeksu)) {
//                return student;
                return Optional.of(student); // Jeśli jest wynik, to zwracamy go przez Optional.of
            }
        }
//        return null;
//        throw new RuntimeException();
        return Optional.empty(); // jeśli nie chcemy / nie mamy wyniku do zwrócenia, to zwracamy empty
    }

    public void usunStudenta(String nrIndeksu) {
        for (Student student : students) {
            if (student.getIndeksNumber().equals(nrIndeksu)) {
                students.remove(student);
                break;
            }
        }
    }

    public boolean czyIstniejeStudentOPodanymIndeksie(String index) {
        return zwrocStudenta(index).isPresent();
    }

    public void listujWszystkichStudentow() {
        for (Student student : students) {
            System.out.println(student);
        }
    }

}
