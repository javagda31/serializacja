package com.j31.serialization.dziennik;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Dziennik dziennik = new Dziennik();
        Scanner scanner = new Scanner(System.in);

        String komenda;

        do {
            System.out.println("Podaj komendę:");
            komenda = scanner.next();

            if (komenda.equalsIgnoreCase("dodaj")) {
                System.out.println("Podaj imie:");
                String imie = scanner.next();

                System.out.println("Podaj nazwisko:");
                String nazwisko = scanner.next();

                System.out.println("Podaj indeks:");
                String indeks = scanner.next();

                Student student = new Student(imie, nazwisko, indeks);
                dziennik.dodajStudenta(student);

                System.out.println("Dodano studenta.");
            } else if (komenda.equalsIgnoreCase("usun")) {
                System.out.println("Podaj indeks:");
                String indeks = scanner.next();

                dziennik.usunStudenta(indeks);

                // TODO: dalsze polecenia napiszecie sami. Ja wylistuję tylko jakie komendy
                //  należy zaimplementować
            } else if (komenda.equalsIgnoreCase("srednia")) {

            } else if (komenda.equalsIgnoreCase("znajdz")) {

            }

        } while (!komenda.equalsIgnoreCase("quit"));
        // ### ZAPIS DANYCH NA DYSK
        dziennik.zapiszNaDysk();
    }
}
