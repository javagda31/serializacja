package com.j31.serialization.dziennik;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Data
public class Student {
    private List<Double> grades = new ArrayList<>();
    private String name;
    private String surname;
    private String indeksNumber;

    public Student() {
    }

    public Student(String name, String surname, String indeksNumber) {
        this.name = name;
        this.surname = surname;
        this.indeksNumber = indeksNumber;
    }

    public void setGrades(List<Double> grades) {
        this.grades = grades;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setIndeksNumber(String indeksNumber) {
        this.indeksNumber = indeksNumber;
    }

    public List<Double> getGrades() {
        return grades;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getIndeksNumber() {
        return indeksNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return Objects.equals(name, student.name) &&
                Objects.equals(surname, student.surname) &&
                Objects.equals(indeksNumber, student.indeksNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, indeksNumber);
    }

    @Override
    public String toString() {
        return "Student{" +
                "grades=" + grades +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", indeksNumber='" + indeksNumber + '\'' +
                '}';
    }
}
